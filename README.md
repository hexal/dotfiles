# Ansible dotfiles configuration (Ubuntu 20.04 LTS)

- Install git
  - `sudo apt install git`
- Install ansible
  - `sudo apt install ansible`
- Install ansible playbook(s)
  - `ansible-playbook -i hosts -K playbook-cli.yml`
  - `ansible-playbook -i hosts -K playbook-gui.yml`
  - `ansible-playbook -i hosts -K playbook-docker.yml`

# CLI

- zsh
- fzf
- vim
- tldr
- python3-pip
- python3-venv
- nodejs
- npm
- heroku
- postgresql and postgresql-contrib

# GUI

- vscode
- insomnia
- snapd
- htop
- rofi
- flameshot
- arandr
- meld
